"use strict";

var AWS = require('aws-sdk');

const userDBArn = process.env['USER_DB'];
const userDBArnArr = userDBArn.split('/');
const userTableName = userDBArnArr[userDBArnArr.length - 1];

// Test package and update (delete comment afterwards)
exports.handleHttpRequest = function(request, context, done) {
  try {
    const userId = request.pathParameters.userId;
    let response = {
      headers: {},
      body: '',
      statusCode: 200
    };

    switch (request.httpMethod) {
      case 'GET': {
        console.log('GET');
        let dynamo = new AWS.DynamoDB();
        var params = {
          TableName: userTableName,
          Key: { 'user_id' : { S: userId } },
          ProjectionExpression: 'email'
        };
        // Call DynamoDB to read the item from the table
        dynamo.getItem(params, function(err, data) {
          if (err) {
            console.log("Error", err);
            throw `Dynamo Get Error (${err})`
          } else {
            console.log("Success", data.Item.email);
            response.body = 'your user info is:\n' + JSON.stringify(data.Item.email);
            done(null, response);
          }
        });
        break;
      }
      case 'POST': {
        console.log('POST');
        let bodyJSON = JSON.parse(request.body || '{}');
        let dynamo = new AWS.DynamoDB();
        let params = {
          TableName: userTableName,
          Item: {
            'user_id': { S: userId },
            'email': { S: bodyJSON['email'] }
          }
        };
        dynamo.putItem(params, function(error, data) {
          if (error) throw `Dynamo Error (${error})`;
          else done(null, response);
        })
        break;
      }
    }
  } catch (e) {
    done(e, null);
  }
}