# ClimJou

## Overview

A simple project to test (if time allows it):  
* Cloud formation
* Simple NodeJs Lambda API
* Simple GraphQL API

## Scenario

Build a simple API for a Climbing Journal. It should simply let a user:  
* add workout items
* visualize a climbing session

## Definitions

* A session is the set of all workouts performed in a day.
* a workout is the set of all workout items performed in a day, for a given workout type:
  * route
  * block
  * core (body building, stretching..)
  * cardio
* workout items can be:  
  * a route
  * a block problem
  * a core exercise
  * a free text note