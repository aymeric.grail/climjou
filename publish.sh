# # zip lambda
# cd handlers 
# rm users-handler.zip 
# zip -X -r ./users-handler.zip *
# aws lambda update-function-code --function-name ClimJou-UsersHandler-18JSD88PDBNR7 --zip-file fileb://users-handler.zip
# cd .. 

# # Udpate stack
# aws cloudformation update-stack --stack-name ClimJou \
#   --template-body file:///$PWD/stack.yaml \
#   --capabilities CAPABILITY_IAM 

# package stack
aws cloudformation package \
  --template stack.yaml \
  --s3-bucket climjou-functions \
  --output json > packaged-template.yaml
  
# deploy stack
aws cloudformation deploy \
  --template-file packaged-template.yaml \
  --stack-name ClimJou \
  --capabilities CAPABILITY_IAM 